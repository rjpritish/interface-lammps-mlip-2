# interface-lammps-mlip-v2

An interface between LAMMPS and MLIP.
MLIP, Machine Learning Interatomic Potentials, is a software distributed
by the group of A. Shapeev from Skoltech (Moscow).
MLIP can be obtained by following the instructions at
https://mlip.skoltech.ru/download/.

Note: current version of the MLIP-LAMMPS interface works with 
LAMMPS-2022 or later. Please, download one of these versions of LAMMPS. 

#### Installation

**0. Prerequisites**

In order to install a MLIP plugin into LAMMPS:

0.1 Create a directory for MLIP-LAMMPS

```mkdir MLIP-LAMMPS && cd MLIP-LAMMPS```

0.2 Clone MLIP-LAMMPS interface

```git clone https://gitlab.com/ashapeev/interface-lammps-mlip-2.git```

0.3 Clone MLIP version 2

```git clone https://gitlab.com/ashapeev/mlip-2.git MLIP```

0.4 Clone the 2022 (or later) version of the LAMMPS code (we recommend using a previously uncompiled version of the code) 

```git clone -b stable https://github.com/lammps/lammps.git mylammps```

**1. Compilations**

1.1 Compile `lib_mlip_interface.a` in the MLIP folder

```cd MLIP && make libinterface```

1.2 Place the compiled `lib_mlip_interface.a` in the `interface-lammps-mlip-2/` directory

```cd .. && cp MLIP/lib/lib_mlip_interface.a interface-lammps-mlip-2/```

1.3 (Optional) If additional LAMMPS packages are necessary they should be included at the end of the preinstall.sh file

```make yes-<package>```

e.g.

```make yes-manybody```

1.4 (Optional) Compilation of lammps mpi requires mpich. If you don't have mpich, you can install it first from here

Download: https://www.mpich.org/downloads/

Installation Guide: https://www.mpich.org/documentation/guides/
 
1.5. Compile LAMMPS from `interface-lammps-mlip-2/` directory  

```./install.sh <path-to-lammps> <lammps-target>```

e.g. Combination of LAMMPS and MLIP settings that work

* MLIP and LAMMPS with default compiler (serial version) `./install.sh ../mylammps serial`
* MLIP and LAMMPS with default compiler (parallel version) `./install.sh ../mylammps mpi`
* MLIP with the GNU compiler and LAMMPS with `./install.sh ../mylammps g++_serial`
* MLIP with the GNU compiler and mpich and LAMMPS with `./install.sh ../mylammps g++_mpich`
* MLIP with Intel compiler and intel MPI and LAMMPS with `./install.sh ../mylammps intel_cpu_intelmpi`
* If you'd like to build with Intel and no MPI then you need to find a way to link LAMMPS with MKL

If everything goes fine, the lammps binary will be copied to this folder.
